# PROGI project teams - frontend

Demo aplikacija React frontenda za potrebe predmeta Programsko inženjerstvo,
na gostujećem predavanju firme [CROZ](http://croz.net).

Prezentaciju možete skinuti sa Sharepointa
- [prezentacija 2024.](https://croz-my.sharepoint.com/:p:/g/personal/mkatanec_croz_net/EeUEZFiQNoBDmKRfT_JjMOIBfb40rtcx10nuXBOTEXO8yA?e=ZCnNj9)

Probleme s projektom ili pitanja možete unijeti kao [issue](https://gitlab.com/jtomic/opp-project-teams-frontend/issues).

## Verzije koda

Kod je verzioniran po godinama te za svaku godinu posotji tag koji predstavlja verziju koda na predavanju te godine
- [`predavanje-2024`](https://gitlab.com/jtomic/opp-project-teams-frontend/-/tags/predavanje-2024).
- [`predavanje-2023`](https://gitlab.com/jtomic/opp-project-teams-frontend/-/tags/predavanje-2023).
- [`predavanje-2022`](https://gitlab.com/jtomic/opp-project-teams-frontend/-/tags/predavanje-2022).
- [`predavanje-2019`](https://gitlab.com/jtomic/opp-project-teams-frontend/-/tags/predavanje-2019).

## Materijali za pomoć pri izradi projekta

Dokumentacija
- [React dokumentacija](https://react.dev/)

Korine knjižnice
- [shadcn/ui](https://ui.shadcn.com/)
- [Tailwind CSS](https://tailwindcss.com/)
- [React Query](https://tanstack.com/query/v3)
- [React Router](https://reactrouter.com/en/main)


### Materijali prijašnjih godina

Video tutoriali s ranijih godina su dostupni na [zasebnoj stranici](https://gitlab.com/jtomic/opp-project-teams-frontend/-/blob/master/education.md).

Prezentacije s ranijih goidna
- [prezentacija 2022.](https://docs.google.com/presentation/d/17wJcYrHrIFVIY5XxEjMTgAnJPp9TgZ3O/edit?usp=sharing&ouid=106389199799474973183&rtpof=true&sd=true) (ažuriran dio s routingom)