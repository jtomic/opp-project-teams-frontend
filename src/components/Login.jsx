import React from 'react';
import Card from "./Card.jsx";
import './Login.css';

function Login(props) {
  const [loginForm, setLoginForm] = React.useState({username: '', password: ''});
  const [error, setError] = React.useState('');

  function onChange(event) {
    const {name, value} = event.target;
    setLoginForm(oldForm => ({...oldForm, [name]: value}))
  }

  function onSubmit(e) {
    e.preventDefault();
    setError("");
    const body = `username=${loginForm.username}&password=${loginForm.password}`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
    };
    fetch('/api/login', options)
      .then(response => {
        if (response.status === 401) {
          setError("Login failed");
        } else {
          props.onLogin();
        }
      });
  }

  return (
    <Card>
      <div className="Login">
        <span>Choose OAuth provider</span>
        <div className="Providers">
          <a href="http://localhost:8080/oauth2/authorization/google">Login with Google</a>
        </div>
      </div>
    </Card>
  )
}

export default Login;



