import * as React from "react";
import "./StudentForm.css"
import Card from "./Card.jsx";
import {useNavigate} from "react-router-dom";

function StudentForm() {
  const navigate = useNavigate();
  const [form, setForm] = React.useState({jmbag: '', givenName: '', familyName: ''});

  function onChange(event) {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  function onSubmit(e) {
    e.preventDefault();
    const data = {
      jmbag: form.jmbag,
      familyName: form.familyName,
      givenName: form.givenName
    };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };

    return fetch('/api/students', options)
      .then(response => {
        if (response.ok) {
          navigate('/students');
        }
      });
  }

  function isValid() {
    const {jmbag, givenName, familyName} = form;
    return jmbag.length === 10 && givenName.length > 0 && familyName.length > 0;
  }

  return (
    <Card title="New student">
      <div className="StudentForm">
        <form onSubmit={onSubmit}>
          <div className="FormRow">
            <label>JMBAG</label>
            <input name='jmbag' onChange={onChange} value={form.jmbag}/>
          </div>
          <div className="FormRow">
            <label>Given name</label>
            <input name='givenName' onChange={onChange} value={form.givenName}/>
          </div>
          <div className="FormRow">
            <label>Family name</label>
            <input name='familyName' onChange={onChange} value={form.familyName}/>
          </div>
          <button type="submit" disabled={!isValid()}>Add student</button>
        </form>
      </div>
    </Card>
  )
}

export default StudentForm;
