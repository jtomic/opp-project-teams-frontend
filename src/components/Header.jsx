import {Link} from "react-router-dom";
import "./Header.css";

function Header(props) {

  function logout() {
    fetch("/api/logout").then(() => {
      props.onLogout();
    });
  }

  return (
    <header className="Header">
      <Link to='/students'>Students</Link>
      <Link to='/students/new'>Add student</Link>
      <button onClick={logout}>Logout</button>
    </header>
  )
}

export default Header;




