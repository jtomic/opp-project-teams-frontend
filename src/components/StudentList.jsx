import * as React from "react";
import Student from './Student.jsx';
import './StudentList.css';
import Card from "./Card.jsx";

function StudentList() {
  const [students, setStudents] = React.useState([]);

  React.useEffect(() => {
    fetch('/api/students')
      .then(data => data.json())
      .then(students => setStudents(students))
  }, []);

  return (
    <Card title="Students">
      {
        students.map(student =>
          <Student key={student.jmbag}
                   student={student}/>
        )
      }
    </Card>
  );
}

export default StudentList;
