import * as React from "react";
import './App.css'
import {createBrowserRouter, Outlet, RouterProvider} from "react-router-dom";
import StudentList from "./components/StudentList.jsx";
import StudentForm from "./components/StudentForm.jsx";
import Header from "./components/Header.jsx";
import Login from "./components/Login.jsx";

function App() {
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [loadingUser, setLoadingUser] = React.useState(true);

  const router = createBrowserRouter([
    {
      path: "/",
      element: <AppContainer onLogout={onLogout}/>,
      children: [
        {
          path: "students",
          element: <StudentList/>
        },
        {
          path: "students/new",
          element: <StudentForm/>
        },
      ]
    }
  ]);

  React.useEffect(() => {
    fetch("/api/user")
      .then(response => {
        setLoadingUser(false);

        if (response.status === 200) {
          setIsLoggedIn(true);
        } else {
          setIsLoggedIn(false);
        }
      })
  }, []);

  if (loadingUser) {
    return <div>Loading...</div>
  }

  function onLogin() {
    setIsLoggedIn(true)
  }

  function onLogout() {
    setIsLoggedIn(false);
  }

  if (!isLoggedIn) {
    return (
      <div className="App">
        <Login onLogin={onLogin}/>
      </div>
    )
  }

  return (
    <RouterProvider router={router}/>
  )
}

export default App

function AppContainer(props) {
  console.log(props)
  return (
    <div>
      <Header onLogout={props.onLogout}/>
      <div className="App">
        <Outlet/>
      </div>
    </div>
  )
}
