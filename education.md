# Izrada frontenda u Reactu

U ovome dokumentu mozete naci niz edukativnih videa koji ce vas nauciti kako kreirati React aplikaciju, napraviti komponente vašeg sučelja, te kako se spojiti na postojeći backend putem RESTa. Videi pretpostavljaju osnovno znanje HTML-a, CSS-a te Javascripta, pa ako se sa njima jos niste susreli, prođite kroz [ove materijale](https://drive.google.com/drive/folders/1K9_MhrqzHjPBgSN8TlQyalwRSydjLrNM).

Video tutoriali se naslanjaju na postojeć backend napravljen u Spring Bootu koji je objasnjen [ovdje](https://gitlab.com/hrvojesimic/opp-project-teams-backend/-/blob/master/education.md)


## Video tutorial izrade

1. [Kreiranje aplikacije](https://drive.google.com/file/d/1L8aO-Nz5oOS1kregjfrCNzk3nxg8CgiW/view?usp=sharing)
2. [Prva komponenta, propovi](https://drive.google.com/file/d/17FoCer4BaUVYZe2FCx8BWyqpt3zn7KGx/view?usp=sharing)
3. [Hookovi](https://drive.google.com/file/d/1kpmqKDf_nktmQzEY6cDRGNDdQQ2ARG5B/view?usp=sharing)
4. [Dohvat sa backenda, forma, validacije](https://drive.google.com/file/d/1EqDDGiE-DBPkWFRzkTXg0EYq-B8xoU--/view?usp=sharing)
5. [React router](https://drive.google.com/file/d/143DBPjd_MaNuUenAgNsodtXv5-t9iODy/view?usp=sharing)
6. [Security, vizualne komponente](https://drive.google.com/file/d/12cJZVh9z61kHZAqwrlIk-rVJpu-sPR73/view?usp=sharing)

## Dodatni materijali

Za rad na projektu dobro će vam doći i ovi javascript libraryi:

### Gotove vizualne komponente:
- [Material UI](https://material-ui.com/)
- [Reactstrap](https://reactstrap.github.io/)
- [Semantic UI](https://react.semantic-ui.com/)
- [Tailwind](https://tailwindcss.com/)

### Forme
- [Formik](https://formik.org/)
- [Yup](https://github.com/jquense/yup)

### Upravljanje globalnim stanjem
- [Redux](https://redux.js.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [MobX](https://mobx.js.org/README.html)

### Rad sa datumima i vremenom
- [date-fns](https://date-fns.org/)

### Pregled komponenata u izolaciji
- [Storybook](https://storybook.js.org/)

### Mobilni razvoj
- [React Native](https://reactnative.dev/)

## Gotov projekt
Stanje programskog kôda na kraju ovog tutoriala možete pronaći pod tagom `predavanje-2019`.
